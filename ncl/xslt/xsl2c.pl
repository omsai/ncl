#!/usr/bin/perl -w
use strict;
#Author: Brandon Chisham
#Date: August 26, 2008
#Description:
#  Script to convert xsl templates into c-header files.

#Convert the template file name to the header file name.
sub get_header_name {
  my $ret = $_[0];
  $ret =~ s/xsl/h/g;
  return $ret;
}
#Create a unique include guard constant.
sub get_include_guard_constant {
  my $ret = $_[0];
  $ret =~ tr/[a-z]/[A-Z]/;
  $ret =~ s/\./_/g;
  $ret .= "_";
  return $ret;
}

#Create opening boiler-plate.
sub get_opening {
  my $ret = "";
  my $include_const = get_include_guard_constant( $_[0] );
  $ret .= "#ifndef " . $include_const . "\n";
  $ret .= "#define " . $include_const . "\n";

  return $ret;
}
#Create closing boiler-plate.
sub get_closing {
  return "#endif\n";
}


my $infile = $ARGV[0];
my $outfile = get_header_name( $infile );

open IN, "<$infile" or die "Unable to open \"$infile\" for reading: \"$!\"\n";
open OUT, ">$outfile" or die "Unable to open \"$outfile\" for writing: \"$!\"\n";

print OUT get_opening( $outfile ) . "\n";
my $out_str = "#define " . get_include_guard_constant( $outfile ) . "STR "; 

while (<IN>){
    chomp($_);
    s/"/\\"/g;
    $out_str .= "\"" . $_ . "\" \\\n"; 
}

close IN;

$out_str =~ s/\\\n$/\n/;

print OUT $out_str;

print OUT get_closing();
close OUT;
