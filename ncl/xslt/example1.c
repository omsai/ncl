#include <string.h>
#include <strings.h>
#include <libxml/xmlmemory.h>
#include <libxml/debugXML.h>
#include <libxml/tree.h>
#include <libxml/xmlIO.h>
#include <libxml/parser.h>
#include <libxml/xinclude.h>
#include <libxml/catalog.h>
#include <libxslt/xslt.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>

#define _GNU_SOURCE

#define MAX_NUM_PARAMS 16
#define MAX_NUM_PARAMS_SIZE (MAX_NUM_PARAMS + 1)
#define MAX_PARAM 1023
#define PARAM_SIZE (MAX_PARAM + 1)
#define KEY_OFFSET 1
#define VALUE_OFFSET 2
#define PARAM_OFFSET 2

extern int xmlLoadExtDtdDefaultValue;

static void usage(const char *name) {
    printf("Usage: %s [options] stylesheet file [file ...]\n", name);
    printf("      --param name value : pass a (parameter,value) pair\n");
}

typedef struct { 
        xsltStylesheetPtr current;
        xmlDocPtr source_doc;
        char result_doc[ PARAM_SIZE ];
        const char* params[ MAX_NUM_PARAMS_SIZE ];
}ProcessorEnvironment_t;

ProcessorEnvironment_t process_args(int argc, char** argv);

int main(int argc, char **argv){
     ProcessorEnvironment_t procEnv = process_args( argc, argv );
     xmlDocPtr result = xsltApplyStylesheet( procEnv.current, procEnv.source_doc, procEnv.params);
     xsltSaveResultToFile( stdout, result, procEnv.current );

     xsltFreeStylesheet( procEnv.current );
     xmlFreeDoc( result );
     xmlFreeDoc( procEnv.source_doc );

     xsltCleanupGlobals();
     xmlCleanupParser();
     return 0;
}


ProcessorEnvironment_t process_args(int argc, char** argv){
   unsigned int curr_param = 0;
   if (argc < 2 ){ usage(argv[0]); exit( 1 ); }
   ProcessorEnvironment_t ret;
   for (unsigned int i = 0; i < argc - 1; ++i){
      if (strncasecmp("--in", argv[ i ], sizeof("--in")/sizeof(char)) == 0 ||  
          strncasecmp("-i", argv[i ], sizeof("-i")/sizeof(char)) == 0 ){
         ret.source_doc = xmlParseFile( argv[ i + 1 ] );      
      }
      else if (strncasecmp("--out", argv[ i ], sizeof("--out")/sizeof(char)) == 0 ||  
               strncasecmp("-o", argv[i ], sizeof("-s")/sizeof(char)) == 0  ){
         strncpy( ret.result_doc, argv[ i + 1 ], MAX_PARAM );
      }
      else if (strncasecmp("--style", argv[ i ], sizeof("--style")/sizeof(char)) == 0 ||
               strncasecmp("-s", argv[i], sizeof("-s")/sizeof(char)) ==0 ) {
         ret.current = xsltParseStylesheetFile( (const xmlChar*)argv[i] );
      }
      else if (strncasecmp("--param", argv[ i ], sizeof("--param")/sizeof(char)) == 0 ||
               strncasecmp("-p", argv[i], sizeof("-p")/sizeof(char)) ==0 ){
                      
         ret.params[ curr_param ]     = argv[ i + KEY_OFFSET ];
         ret.params[ curr_param + 1 ] = argv[ i + VALUE_OFFSET ];

         curr_param += PARAM_OFFSET;
          
      }

   }
   return ret;
}

