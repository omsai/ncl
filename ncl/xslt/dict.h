#ifndef DICT_H_
#define DICT_H_

#define DICT_H_STR "<?xml version=\"1.0\"?>" \
"<xsl:stylesheet version=\"1.0\"" \
"                xmlns:xsl= \"http://www.w3.org/1999/XSL/Transform\"" \
"                xmlns:nex=\"http://www.nexml.org/1.0\"" \
"                xmlns=\"\">" \
"  <xsl:output method=\"text\"" \
"              encoding=\"string\"" \
"              omit-xml-declaration=\"yes\"" \
"              indent=\"no\" />" \
"  <xsl:template match=\"/\">" \
"     " \
"     <xsl:for-each select=\"nex:nexml/dict\">" \
"          <xsl:text>&#34;</xsl:text>" \
"          <xsl:value-of select=\"key/text()\" />" \
"          <xsl:text>&#34; &#34;</xsl:text>" \
"          <xsl:value-of select=\"string/text()\" />" \
"          <xsl:text>&#34; </xsl:text>" \
"          <xsl:text>&#10;</xsl:text>" \
"     </xsl:for-each>" \
"    " \
"  </xsl:template>" \
"</xsl:stylesheet>" ;
#endif
