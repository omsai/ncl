#include "nclxsltxx.hpp"
using namespace std;
/*
 * Extract an attribute list from an element.
 */
map< string, string > get_attribute_list( xmlNode* in){
   xmlNode* current;
   map< string, string > ret = map< string, string >();
   for (current = in; current && current->type == XML_ATTRIBUTE_NODE; current = current->next){
         ret[ (const char*)current->name ]  = (const char*)xmlNodeGetContent( current ) ;
   }
   return ret;
}
