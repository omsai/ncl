#ifndef TAXA_H_
#define TAXA_H_

#define TAXA_H_STR "<?xml version=\"1.0\"?>" \
"<xsl:stylesheet version=\"1.0\"" \
"                xmlns:xsl= \"http://www.w3.org/1999/XSL/Transform\"" \
"                xmlns:nex=\"http://www.nexml.org/1.0\"" \
"                xmlns=\"\">" \
"                <xsl:output method=\"xml\"" \
"                        encoding=\"string\"" \
"                        omit-xml-declaration=\"no\"" \
"                        indent=\"yes\" />" \
"" \
"  <xsl:template match=\"/\">" \
"     <!-- Select the list of otus -->" \
"     <otus>" \
"     <xsl:text>&#10;</xsl:text>" \
"     <xsl:for-each select=\"nex:nexml/otus/otu\">" \
"          <otu><xsl:value-of select=\"@label\" /></otu>" \
"          <xsl:text>&#10;</xsl:text>" \
"     </xsl:for-each>" \
"     </otus>" \
"  </xsl:template>" \
"</xsl:stylesheet>" 
#endif
