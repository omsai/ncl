#include <string.h>
#include <strings.h>
#include <libxml/xmlmemory.h>
#include <libxml/debugXML.h>
#include <libxml/tree.h>
#include <libxml/xmlIO.h>
#include <libxml/parser.h>
#include <libxml/xinclude.h>
#include <libxml/catalog.h>
#include <libxslt/xslt.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>
#include <stdio.h>

#define _GNU_SOURCE

#define MAX_NUM_PARAMS 16
#define MAX_NUM_PARAMS_SIZE (MAX_NUM_PARAMS + 1)
#define MAX_PARAM 1023
#define PARAM_SIZE (MAX_PARAM + 1)
#define KEY_OFFSET 1
#define VALUE_OFFSET 2
#define PARAM_OFFSET 2

extern int xmlLoadExtDtdDefaultValue;
/*
 * Print a short message when required parameters are not present.
 */
static void usage(const char *name) {
    printf("Usage: %s [options] stylesheet file [file ...]\n", name);
    printf("      --param name value : pass a (parameter,value) pair\n");
}
/*
 * Represents global parameters and the results of the command argument processing.
 */
typedef struct { 
        xsltStylesheetPtr current;
        xmlDocPtr source_doc;
        char result_doc[ PARAM_SIZE ];
        const char* params[ MAX_NUM_PARAMS_SIZE ];
}ProcessorEnvironment_t;
/*
 * Process the command-line arguments. 
 */
ProcessorEnvironment_t* process_args(int argc, char** argv);
/*
 * Print the result tree.
 */
void print_tree( xmlNode* tree, unsigned int level );

int main(int argc, char **argv){
    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION
  printf("Starting up...\n");
  
  ProcessorEnvironment_t* procEnv = process_args( argc, argv );

  printf("Initialized Environment\n");

  xmlDocPtr result = xsltApplyStylesheet( procEnv->current, procEnv->source_doc, procEnv->params);
  
  printf("Applied stylesheet\n");
  //xsltSaveResultToFile( stdout, result, procEnv->current );
  
   
  print_tree( xmlDocGetRootElement( result ), 0 );
 
  printf("Printed the tree.\n");
  xsltFreeStylesheet( procEnv->current );



  xmlFreeDoc( result );
  xmlFreeDoc( procEnv->source_doc );

  xsltCleanupGlobals();
  xmlCleanupParser();
  
  free(procEnv);
  return 0;
}


ProcessorEnvironment_t* process_args(int argc, char** argv){
   unsigned int curr_param = 0;
   if (argc < 2 ){ usage(argv[0]); exit( 1 ); }
   ProcessorEnvironment_t* ret = (ProcessorEnvironment_t*)calloc( 1, sizeof(ProcessorEnvironment_t));
   for (unsigned int i = 0; i < argc - 1; ++i){
      if (strncasecmp("--in", argv[ i ], sizeof("--in")/sizeof(char)) == 0 ||  
          strncasecmp("-i", argv[ i ], sizeof("-i")/sizeof(char)) == 0 ){
         
         //printf("\tinputfile: %s\n", argv[ + + 1 ]);
         
         ret->source_doc = xmlParseFile( argv[ i + 1 ] );      
      }
      else if (strncasecmp("--out", argv[ i ], sizeof("--out")/sizeof(char)) == 0 ||  
               strncasecmp("-o", argv[ i ], sizeof("-o")/sizeof(char)) == 0  ){
         
         //printf("\toutputfile: %s\n", argv[ i + 1 ]);
         
         strncpy( ret->result_doc, argv[ i + 1 ], MAX_PARAM );
      }
      else if (strncasecmp("--style", argv[ i ], sizeof("--style")/sizeof(char)) == 0 ||
               strncasecmp("-s", argv[ i ], sizeof("-s")/sizeof(char)) ==0 ) {
         
         //printf("\tstylesheet: %s\n", argv[ i + 1 ]);
         
         ret->current = xsltParseStylesheetFile( (const xmlChar*)argv[i + 1] );
      }
      else if (strncasecmp("--param", argv[ i ], sizeof("--param")/sizeof(char)) == 0 ||
               strncasecmp("-p", argv[ i ], sizeof("-p")/sizeof(char)) ==0 ){
                      
         ret->params[ curr_param ]     = argv[ i + KEY_OFFSET ];
         ret->params[ curr_param + 1 ] = argv[ i + VALUE_OFFSET ];

         curr_param += PARAM_OFFSET;
          
      }

   }
   return ret;
}


void print_tree( xmlNode* tree, unsigned int level ){

  //printf("print_tree( tree:%p, level:%d )\n", tree, level);

  xmlNode* current_node = NULL;
  for (current_node = tree; current_node; current_node = current_node->next){
    if (current_node->type == XML_ELEMENT_NODE){
      for (unsigned int clevel = 0; clevel < level; ++clevel ){ printf("  "); }
          printf("Node name: %s\n", current_node->name);
          print_tree(current_node->children, level+1);
       }
  }
  return;
}

