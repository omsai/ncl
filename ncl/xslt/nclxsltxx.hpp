#ifndef NCLXSLT_HPP_
#define NCLXSLT_HPP_
#include <map>
#include <string>
#include "nclxslt.h"


/*
 * Extracts the source, target, and lenght attributes of the edge.
 */
std::map<std::string,std::string>  get_attribute_list( xmlNode* );


#endif
