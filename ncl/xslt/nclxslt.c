#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "nclxslt.h"

#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <limits.h>
#include <unistd.h>

#ifndef PATH_MAX
#define PATH_MAX 1024
#endif

/*
 * Author: Brandon Chisham
 * Date: August 26, 2008
 * Implementation of the basic xslt processor interface. 
 */

void print_tree_impl( xmlNode* tree, ProcessorEnvironment_t* env, unsigned int level );

ProcessorEnvironment_t* init_processor_env( xsltStylesheetPtr style, xmlDocPtr source, char result_doc[], const char* params[]){
   //allocate to an initialzed block
   ProcessorEnvironment_t* ret = (ProcessorEnvironment_t*)calloc( 1, sizeof(ProcessorEnvironment_t));
   //change default 
   ret->current = style;
   ret->source_doc = source;
   memcpy(ret->result_doc, result_doc, PARAM_SIZE);
   memcpy(ret->params, params, MAX_NUM_PARAMS_SIZE * sizeof(const char*));
   return ret;
}

void destroy_processor_env(ProcessorEnvironment_t* env){
   xsltFreeStylesheet( env->current );
   xmlFreeDoc( env->source_doc );
   free( env );

}
/*
ProcessorEnvironment_t* process_args(int argc, char** argv){
   unsigned int curr_param = 0;
   //require both stylesheet and xml-instance files.
   if (argc < 2 ){ usage(argv[0]); exit( 1 ); }
   ProcessorEnvironment_t* ret = (ProcessorEnvironment_t*)calloc( 1, sizeof(ProcessorEnvironment_t));
   for (unsigned int i = 0; i < argc - 1; ++i){
     //check for particular flags.  
     if (strncasecmp("--in", argv[ i ], sizeof("--in")/sizeof(char)) == 0 ||  
          strncasecmp("-i", argv[ i ], sizeof("-i")/sizeof(char)) == 0 ){
         
         //printf("\tinputfile: %s\n", argv[ + + 1 ]);
         
         ret->source_doc = xmlParseFile( argv[ i + 1 ] );      
      }
      else if (strncasecmp("--out", argv[ i ], sizeof("--out")/sizeof(char)) == 0 ||  
               strncasecmp("-o", argv[ i ], sizeof("-o")/sizeof(char)) == 0  ){
         
         //printf("\toutputfile: %s\n", argv[ i + 1 ]);
         
         strncpy( ret->result_doc, argv[ i + 1 ], MAX_PARAM );
      }
      else if (strncasecmp("--style", argv[ i ], sizeof("--style")/sizeof(char)) == 0 ||
               strncasecmp("-s", argv[ i ], sizeof("-s")/sizeof(char)) ==0 ) {
         
         //printf("\tstylesheet: %s\n", argv[ i + 1 ]);
         
         ret->current = xsltParseStylesheetFile( (const xmlChar*)argv[i + 1] );
      }
      //an incomplete parameter value specification will crash the program. (segfault) here.
      //if it is the last argument. 
      else if (strncasecmp("--param", argv[ i ], sizeof("--param")/sizeof(char)) == 0 ||
               strncasecmp("-p", argv[ i ], sizeof("-p")/sizeof(char)) ==0 ){
                      
         ret->params[ curr_param ]     = argv[ i + KEY_OFFSET ];
         ret->params[ curr_param + 1 ] = argv[ i + VALUE_OFFSET ];

         curr_param += PARAM_OFFSET;
      }
   }
   return ret;
}
*/
void print_tree( xmlNode* tree, ProcessorEnvironment_t* env ){
  print_tree_impl(tree, env, 0);
  return;
}

void print_tree_impl( xmlNode* tree, ProcessorEnvironment_t* env, unsigned int level ){

  //printf("print_tree( tree:%p, level:%d )\n", tree, level);

  xmlNode* current_node = NULL;
  //traverse the tree pre-order, and print all the element nodes, and their contents.
  for (current_node = tree; current_node; current_node = current_node->next){
    if (current_node->type == XML_ELEMENT_NODE){
      //indent each level for readability.
      for (unsigned int clevel = 0; clevel < level; ++clevel ){ printf("  "); }
      //see of the node is one that we are interested in printing out.
      if ( (*(env->filter))( current_node ) ){
         print_node( current_node );
      }
      //process the children
      print_tree_impl(current_node->children, env, level+1);
    }
  }
  return;
}

void print_node(xmlNode* current_node){
  //print a node and its contents. 
  if (current_node){
    printf("Node name: \"%s\", Content: \"%s\"\n", current_node->name, xmlNodeGetContent(current_node));
  }
  return;
}
/*
 * Traverse the tree in pre-order.
 */
void traverse( xmlNode* tree, NodeFilter_t filter, NodeProcessor_t processor){
     xmlNode* current_node = NULL;
     for (current_node = tree; current_node; current_node = current_node->next){
       //only visit element nodes. 
       if ( XML_ELEMENT_NODE == current_node->type ){
          //filter the nodes based on client-specified criteria
          if ((*filter)(current_node)){ (*processor)( current_node); }
          //process the children
          traverse(current_node->children, filter, processor);
       }
     }
     return;
}
/*
 * Load and initialize the specifed transform.
 */
xsltStylesheetPtr mktemp_xslt_file( char* transform ){
    //dump the transform to a temporary file.
    char* retname = (char*)calloc( PATH_MAX + 1, sizeof(char));
    if ( NULL == retname ){ perror("Unable to allocate memory for the temp file name");  exit( 1 );}
    strncpy( retname, "xsltXXXXXX", sizeof("xsltXXXXXX"));
    int fdes = mkstemp( retname );
    if ( fdes < 0 ){ perror("unable to opeen temp file"); exit( 1 ); }
    //use standard io instead of the bare descriptor.
    FILE* tout = fdopen( fdes, "w+" );
    if ( NULL == tout ){ perror("unable to open the temp descriptor"); exit( 1 ); }
    fwrite( transform, sizeof(char), strlen(transform), tout );
    fclose( tout );//closes the descriptor too
    //package results.
    xsltStylesheetPtr ret =  xsltParseStylesheetFile( (const xmlChar*)retname );
    //cleanup
    unlink( retname );
    free( retname );
    return ret;
}

