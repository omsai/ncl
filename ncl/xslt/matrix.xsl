<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl= "http://www.w3.org/1999/XSL/Transform"
                xmlns:nex="http://www.nexml.org/1.0"
                xmlns="">
                <xsl:output method="xml"
                        encoding="string"
                        omit-xml-declaration="no"
                        indent="yes" />

  <xsl:template match="/">
      <!-- One nexml instance may contain multiple matrices -->
      <matrices>
      <xsl:for-each select="nex:nexml/characters"> 
         <!-- Mark an individual matrix. -->
         <matrix>
         <xsl:text>&#10;</xsl:text>
          <id><xsl:value-of select="@id" /></id>
          <xsl:text>&#10;</xsl:text>
          <!-- collect data for each row. -->
          <xsl:for-each select="matrix/row">
             <row>
               <xsl:text>&#10;</xsl:text>
               <otu><xsl:value-of select="@otu"/></otu>               
               <xsl:choose>
                  <xsl:when test="seq/text()"> 
                     <data>
                        <xsl:value-of select="seq/text()" />
                      </data>
                   </xsl:when>
                   <xsl:when test="cell">
                      <cellwise-data>
                         <xsl:for-each select="cell">
                         <cell>
                            <character><xsl:value-of select="@char" /></character>
                            <state><xsl:value-of select="@state" /></state>
                         </cell>
                         <xsl:text>&#10;</xsl:text>
                         </xsl:for-each>
                       </cellwise-data>
                     </xsl:when>
                </xsl:choose>
             </row>
             <xsl:text>&#10;</xsl:text>
            </xsl:for-each>
          </matrix>
     </xsl:for-each>
     </matrices>
  </xsl:template>
</xsl:stylesheet>
