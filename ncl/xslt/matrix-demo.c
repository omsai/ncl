#include "nclxslt.h"


bool isOfInterestMatrix( xmlNode* current_node );

int main(int argc, char **argv){
    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION
  printf("Starting up...\n");
  
  ProcessorEnvironment_t* procEnv = process_args( argc, argv );
  procEnv->filter = isOfInterestMatrix;

  printf("Initialized Environment\n");

  xmlDocPtr result = xsltApplyStylesheet( procEnv->current, procEnv->source_doc, procEnv->params);
  
  printf("Applied stylesheet\n");
  xsltSaveResultToFile( stdout, result, procEnv->current );
  
   
  print_tree( xmlDocGetRootElement( result ), procEnv );
 
  printf("Printed the tree.\n");
  xsltFreeStylesheet( procEnv->current );



  xmlFreeDoc( result );
  xmlFreeDoc( procEnv->source_doc );

  xsltCleanupGlobals();
  xmlCleanupParser();
  
  free(procEnv);
  return 0;
}



bool isOfInterestMatrix( xmlNode* current_node ){
    static char* MATRIX_ELTS[] = {"otu", "data", "character", "state"};
    static unsigned int LENS[]  =  {sizeof("otu"), sizeof("data"), sizeof("character"), sizeof("state")};
    static unsigned int LEN   = sizeof(MATRIX_ELTS)/sizeof(MATRIX_ELTS[0]);
    bool ret = false;
    
    for (unsigned int i =0; i < LEN && !ret; ++i){
       ret = strncmp( current_node->name, MATRIX_ELTS[ i ], LENS[ i ]  ) == 0;
    }
    return ret;
}
