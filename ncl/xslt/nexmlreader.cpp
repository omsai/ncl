#include "nclnexml.hpp"
#include <string>

using namespace std;

struct ReaderEnvironment {
   ReaderEnvironment( string instr = ""):in(instr){}
   string in;
};

ReaderEnvironment process_args( int argc, char** argv, char** env);

 

int main( int argc, char** argv, char** env){
  ReaderEnvironment renv = process_args( argc, argv, env );
  NxsNexml( renv.in );
  return 0;
}

ReaderEnvironment process_args( int argc, char** argv, char** env){
    ReaderEnvironment ret;
    if ( argc < 1  ){ usage("nexmlreader nexml_input.xml"); }
    else {  
        ret.in =  argv[ 1 ];;
    }
    return ret;
}
