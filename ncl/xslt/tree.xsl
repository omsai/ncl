<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl= "http://www.w3.org/1999/XSL/Transform"
                xmlns:nex="http://www.nexml.org/1.0"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 	        xmlns:xml="http://www.w3.org/XML/1998/namespace"
	        xsi:schemaLocation="http://www.nexml.org/1.0 http://www.nexml.org/nexml/xsd/nexml.xsd"
                xmlns="">
                <xsl:output method="xml"
                        encoding="string"
                        omit-xml-declaration="no"
                        indent="yes" />

  <xsl:template match="/">
      <!-- Extract the trees component of the NeXML file -->
       <xsl:for-each select="nex:nexml/trees">
         <xsl:for-each select="tree">
           <tree label="{@label}" rooted="true"><xsl:text>&#10;</xsl:text>
              <!-- Extract edges from the tree.  -->
              <xsl:for-each select="edge">
               <edge source="{@source}" target="{@target}" length="{@length}" />
               <xsl:text>&#10;</xsl:text>
            </xsl:for-each><!-- end for each edge -->
           </tree><xsl:text>&#10;</xsl:text>
         </xsl:for-each><!-- end for each tree. -->
         <!-- Transform "network" to "tree" element with an additional attribute -->
         <xsl:for-each select="network">
             <tree label="{@label}" rooted="false"><xsl:text>&#10;</xsl:text>
                 <xsl:for-each select="edge">
                 <edge source="{@source}" target="{@target}" length="{@length}" />
                  <xsl:text>&#10;</xsl:text>
                 </xsl:for-each><!-- end for each edge -->
             </tree><xsl:text>&#10;</xsl:text>
         </xsl:for-each><!-- end for each network -->
      </xsl:for-each><!-- end for each trees -->
  </xsl:template>
</xsl:stylesheet>
