#ifndef NCL_XSLT_H_
#define NCL_XSLT_H_
/*
 * Author: Brandon Chisham
 * Created: August 26, 2008
 * Common defintions needed by the other sub-components.
 */


#include <libxml/xmlmemory.h>
#include <libxml/debugXML.h>
#include <libxml/tree.h>
#include <libxml/xmlIO.h>
#include <libxml/parser.h>
#include <libxml/xinclude.h>
#include <libxml/catalog.h>
#include <libxslt/xslt.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
#include <libxslt/xsltutils.h>
#include <stdbool.h>


#define MAX_NUM_PARAMS 16
#define MAX_NUM_PARAMS_SIZE (MAX_NUM_PARAMS + 1)
#define MAX_PARAM 1023
#define PARAM_SIZE (MAX_PARAM + 1)
#define KEY_OFFSET 1
#define VALUE_OFFSET 2
#define PARAM_OFFSET 2

extern int xmlLoadExtDtdDefaultValue;
/*
 * Print a short message when required parameters are not present.
 */
static void usage(const char *name) {
    printf("Usage: %s [options] stylesheet file [file ...]\n", name);
    printf("      --param name value : pass a (parameter,value) pair\n");
}
/*
 * Signiture for node filter call-backs.
 * The filter callback is called by the "traverse"
 * function to select nodes of particular interest.
 */
typedef bool (*NodeFilter_t)(xmlNode*);
/*
 * Siniture for node processing call-backs. 
 * The "traverse" function invokes this call-back
 * on each node selected by the filter call-back.
 */
typedef void (*NodeProcessor_t)(xmlNode*);

/*
 * Represents global parameters and the results of the command argument processing.
 * Used by the demo program not used by the nexml library classes.
 */
typedef struct { 
        xsltStylesheetPtr current;
        xmlDocPtr source_doc;
        char result_doc[ PARAM_SIZE ];
        const char* params[ MAX_NUM_PARAMS_SIZE ];
        NodeFilter_t filter;
}ProcessorEnvironment_t;
/*
 * Initialize a processor environment. 
 */
ProcessorEnvironment_t* init_processor_env( xsltStylesheetPtr style, xmlDocPtr source, char result_doc[ PARAM_SIZE ], const char* params[ MAX_NUM_PARAMS_SIZE ]);
/*
 * Processor environment destructor.
 */
void destroy_processor_env(ProcessorEnvironment_t* env);
/*
 * Process the command-line arguments. 
 */
//ProcessorEnvironment_t* process_args(int argc, char** argv);
/*
 * Print a data node's contents.
 */
void print_node(xmlNode* current_node);
/*
 * Print the result tree.
 */
void print_tree( xmlNode* tree, ProcessorEnvironment_t* env );
/*
 * Traverses the tree applying processor to nodes matched by filter.
 * Visits all nodes in the document tree in pre-order, and applies
 * the processor call-back to each node for which the filter call-back
 * returns true.
 */
void traverse( xmlNode* tree, NodeFilter_t filter, NodeProcessor_t processor); 
/*
 * Save the transform to a temp file, return a pointer to the stylesheet.
 */
xsltStylesheetPtr mktemp_xslt_file( char* transform );

#endif
